import unittest
import chiffrage_cesar
import verification

class TestChiffrageFonction(unittest.TestCase):
    def test_chiffrage(self):
        self.assertEqual(chiffrage_cesar.chiffrage('abc',8,1), 'ijk')
        self.assertEqual(chiffrage_cesar.chiffrage('abc',8,1), 'ijk')

    def test_dechiffrage(self):
        self.assertEqual(chiffrage_cesar.dechiffrage('c',1,1), 'b')

    def test_verification(self):
        self.assertEqual(verification.Verification("abc",0), 'La valeur de décalage ne doit pas être 0')
        self.assertEqual(verification.Verification("",2), 'Veuillez entrez une chaine')
