# Le chiffrage de César

**Chiffrage et déchiffrage d'une chaine de caractère en Python**

Mini-Projet de 2ème année de DUT consistant à créer un système de chiffrage pour une chaine de caractère en langage Python.

======================
Objectif:
    Chiffrer et déchiffrer une chaine en utilisant la méthode de chiffrage de César, le programme devra comporter une IHM en Tkinter.