#Fonction de chiffrage/déchiffrage des chaines.

import verification
import string

#string.ascii_lowercase

#Fonction de transciption de la chaine
def transcription(chaine,valeurDecalage,mode,circulaire):
    codeascii=0
    chainechiffrer=''

    for x in chaine:
        if (mode=="chiffrage"):
            codeascii=ord(x)+valeurDecalage
            if(not(circulaire.get())):
                chainechiffrer+=chr(codeascii)
            else:
                flag=False
                if(codeascii>ord('Z') and codeascii<ord('a')):
                    finalphabet=ord('Z')
                    debutalphabet=ord('A')
                    flag=True
                if (codeascii>ord('z') and codeascii>ord('a')):
                    finalphabet=ord('z')
                    debutalphabet=ord('a')
                    flag=True
                if(flag==True):
                    temp=codeascii-finalphabet-1
                    chainechiffrer+=chr(debutalphabet+temp)
                else:
                    chainechiffrer+=chr(codeascii)
        elif (mode=="dechiffrage"):
            codeascii=ord(x)-valeurDecalage
            if(not(circulaire.get())):
                chainechiffrer+=chr(codeascii)
            else:
                flag=False
                if(codeascii<ord('A')):
                    finalphabet=ord('Z')
                    debutalphabet=ord('A')
                    flag=True
                if codeascii<ord('a'):
                    finalphabet=ord('z')
                    debutalphabet=ord('a')
                    flag=True

                if(flag==True):
                    temp=debutalphabet-codeascii-1
                    chainechiffrer+=chr(finalphabet-temp)

    return chainechiffrer

#Fonction de chiffrage de la chaine
def chiffrage(chaine,valeurDecalage,circulaire):
    resultatverif=verification.Verification(chaine,valeurDecalage)
    if(resultatverif==""):
        return transcription(chaine,valeurDecalage,"chiffrage",circulaire)
    else:
        return resultatverif


#Fonction de dechiffrage de la chaine
def dechiffrage(chaine,valeurDecalage,circulaire):
    resultatverif=verification.Verification(chaine,valeurDecalage)
    if(resultatverif==""):
        return transcription(chaine,valeurDecalage,"dechiffrage",circulaire)
    else:
        return resultatverif