from tkinter import * 
from tkinter.messagebox import showinfo, showwarning
import chiffrage_cesar
import verification

    #Verifie la validité des paramètres saisie
def check_validity():
    try:
        longueur.set(int(longueur.get()))
    except:
        showwarning(title="ERREUR", message="Le type de la clé de chiffrage est incorrect")
        return False

    coderetour=verification.Verification(chaine.get(),longueur.get())
    if (coderetour!=""):
        showwarning(title="ERREUR", message=coderetour)
        return False
    return True

    # Retranscris la chaine de caractère avec la fonction de chiffrage
def callback_chiffrage():
    if(check_validity()):
        label.config(text=("Chaine chiffrée: %s" % chiffrage_cesar.chiffrage(chaine.get(),longueur.get(),circulaire)))

    # Retranscris la chaine de caractère avec la fonction de dechiffrage
def callback_dechiffrage():
    if check_validity():
        label.config(text=("Chaine dechiffrée: %s" % chiffrage_cesar.dechiffrage(chaine.get(),longueur.get(),circulaire)))


# DEBUT DU PROGRAMME
root =Tk()
root.title("Le chiffrage de César")
root.rowconfigure(3, weight=1)
root.columnconfigure(2, weight=1)

longueur = IntVar()
longueur.set("")

label_longueur = Label(root, text='Saisir la clé de chiffrage')
label_longueur.grid(row=0, column=0)

chaine = StringVar() 
chaine.set("")

label_chaine = Label(root, text='Saisir le texte à transcrire')
label_chaine.grid(row=1, column=0)

longueurDecalage = Entry(root,
                        textvariable = longueur,
                        width=50
                        )
longueurDecalage.grid(row=0,column=1)

entry = Entry(root, 
              textvariable=chaine,
              width=50)
entry.grid(row=1, column=1)

txt = StringVar()
txt.set("")

label =Label(root, text=txt.get())
label.grid(row =2,column=0)


    # Bouton d'action (chiffrage, dechiffrage et quitter)

bouton_chiffrage = Button(root,
                         text='Chiffrage',
                         command=callback_chiffrage)
bouton_chiffrage.grid(row = 3, column = 0)

bouton_dechiffrage = Button(root,
                            text='dechiffrage',
                            command=callback_dechiffrage)
bouton_dechiffrage.grid(row = 3, column = 1)

bouton_quitter = Button(root,
                         text='Quitter',
                         command=root.quit)
bouton_quitter.grid(row = 3, column = 2)


    # RadioBouton - Circulaire ou po

circulaire=BooleanVar()
circulaire.set(False)

radiobouton_circulaire= Radiobutton(root,
                                    text="Circulaire",
                                    value=True,
                                    variable=circulaire)
radiobouton_circulaire.grid(row = 2, column =1)

radiobouton_noncirculaire= Radiobutton(root,
                                    text="Non circulaire",
                                    value=False,
                                    variable=circulaire)
radiobouton_noncirculaire.grid(row = 2, column =2)

root.mainloop()

# FIN DU PROGRAMME
